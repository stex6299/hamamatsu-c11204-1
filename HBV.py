#!/usr/bin/env python3
"""
This script demonstrates how to use the HBV command to set a temporary 
(i.e. non in the EEPROM) tension

This command requires 4 bytes of data, as shown below
"""

from lib_c11204 import *
import serial

# Tensione che voglio settare
Vb = 61 # Volt

# Converto in ADC e tronco a 4 caratteri
Vb_ADC = f"{(Tension_VtoADC(Vb) & 0xffff):04X}"


binToWrite = encodeCommand("HBV", Vb_ADC)

print(f"Scriverò \n{binToWrite}")

with serial.Serial() as ser:
    
    # Serial port settings
    ser.baudrate = 38400
    ser.port = 'COM7'
    
    ser.parity = serial.PARITY_EVEN
    ser.bytesize = serial.EIGHTBITS
    ser.stopbits = serial.STOPBITS_ONE
    ser.timeout = 15
    
    # Open serial port
    ser.open()
    
    # Write my command
    ser.write(binToWrite)

    #s = ser.read(40)
    time.sleep(1)
    bytesToRead = ser.inWaiting()
    s = ser.read(bytesToRead)
    print(f"s: --> {s}")

    
print("Ho scritto")
