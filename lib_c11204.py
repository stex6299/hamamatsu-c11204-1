"""
Author: Stefano Carsi (scarsi@studenti.uninsubria.it)
Date: 2023 06 21

The purpose of this library is to control the Hamamatsu C 11204-01 High Voltage chip
"""

import numpy as np
import sys, os, re, glob, time
import serial
import binascii

def encodeCommand(command: str, data: str) -> bytearray:
    """This function takes as input the command and the relative data, and constructs the bytearray
    object that can be written via the serial port

    Args:
        command (str): Command to be sent to the chip. Usually 3 char (mind the capital letters!!)
        data (str): ASCII representation of the hex value to be sent. The length depends on each command

    Returns:
        bytearray: bytearray object that is to be written on serial port
    """
    
    # Constants of the command
    STX = "02"  # Inizio standard
    ETX = "03"  # Da mettere dopo i dati
    CR = "0D"   # Termine del comando da trasmettere
    
    
    # Initialize variables
    Checksum = 0        # Checksum che calcolo e che poi inserisco
    strToWrite = ""     # Stringa che encoderò
    
    
    
    # Start of text
    Checksum += int(STX, 16)
    strToWrite += STX
        
    
    # Loop over the commadn
    for c in command:
        
        # Hex repr. of each given character -> Str
        hc = c.encode("utf-8").hex()
        
        # Append hex repr. to the command
        strToWrite += hc
        
        # Sum for computing checksum
        Checksum += int(hc, 16)
        
    # Ciclo sui dati 
    # TODO: Decidere se mettere controllo su caratteri maiuscoli qua
    for d in data:
        # Hex repr. of each given character -> Str
        hd = d.upper().encode("utf-8").hex()
        
        # Append hex repr. to the command
        strToWrite += hd
     
        # Sum for computing checksum
        Checksum += int(hd, 16)
    
    
    # End of text
    Checksum += int(ETX, 16)
    strToWrite += ETX
    
    
    # Last two bytes of the checksum
    Checksum = f"{Checksum:04X}"#.upper()    # Check that 04x is fine
    CS1 = Checksum[-2].encode("utf-8").hex()
    CS2 = Checksum[-1].encode("utf-8").hex()
    
    
    # Last part of the command   
    strToWrite += CS1   
    strToWrite += CS2   
    strToWrite += CR   
     
    # Codifico la stringa da scrivere
    return bytearray.fromhex(strToWrite)



# Conversion factor between ADC and Volt
Tension_conversionFactor = 1.812e-3


def Tension_VtoADC(tension: float) -> int:
    """Converts a tension Volt to ADC unit (range: 0-65535) 

    Args:
        tension (float): Tension in Volt unit that is to be converted in ADC

    Returns:
        int: Tension converted in ADC (range: 0-65535)
    """
    
    enc_Vb = np.round( tension / Tension_conversionFactor ).astype(int)
    #hex_Vb = f"{(enc_Vb & 0xffff):04x}"
    
    if enc_Vb > 65535:
        raise Exception("Out of manual range... I don't know what this means...")
    
    return enc_Vb


def Tension_ADCtoV(tension: int) -> float:
    """Converts a tension from ADC unit (range: 0-65535) to Volt

    Args:
        tension (int): Tension in ADC unit  that is to be converted in Volt

    Returns:
        float: Tension converted in Volt 
    """
    
    return tension * Tension_conversionFactor
    

def Temperature_CtoADC(temperature: float) -> int:
    """Converts a temperature from ADC units to Celsius

    Args:
        temperature (float): Temperature value in °C that is to be converted in ADC

    Returns:
        int: Temperature converted in ADC
    """
    
    # Direct formula
    #return (temperature * 1.907e-5 - 1.035) / (-5.5e-3)
    return np.round( (-5.5e-3 * temperature + 1.035)/1.907e-5 ).astype(int)


def Temperature_ADCtoC(temperature: int) -> float:
    """Converts a temperature from Celsius to ADC units 

    Args:
        temperature (int): Temperature value in ADC that is to be converted in °C

    Returns:
        float: Temperature converted in °C
    """
    
    return (temperature * 1.907e-5 - 1.035) / (-5.5e-3)
