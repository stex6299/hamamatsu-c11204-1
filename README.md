# What's here
In this repository there is [a library](./lib_c11204.py) and few example of how to use it to control the *Hamamatsu C 11204-01* High Voltage module

- [HBV.py](./HBV.py): Script for setting a temporary tension (not written on EEPROM)
- [HST.py](./HST.py): Script for writing in the EEPROM the reference voltage, temperature and temperature compensation coefficients


[This link](https://github-wiki-see.page/m/Zethian/C11204-01/wiki/How-to-use-C11204-01-code) might be useful: it is the only place in the entire web where I managed to find the [command reference manual](https://confluence.slac.stanford.edu/download/attachments/213887199/K29-B61218Ke_C11204-01Command%20Reference.pdf?api=v2)

# What still have to be done
- Implement a function, and then a script, for monitoring the situation (HPO command)
- Maybe implement the error code in my library
- Fix the serial read, that at the moment goes in timeout