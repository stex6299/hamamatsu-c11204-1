import numpy as np
import sys, os, re, glob
import serial
import binascii


# Parametro da scrivere
Vb = 61

# Lo codifico come "descritto" nel manuale
# enc_Vb = np.round(Vb * 65536 / 118.75 ).astype(int)
enc_Vb = np.round(Vb / 1.812e-3 ).astype(int)
hex_Vb = f"{(enc_Vb & 0xffff):04x}"
print(f"hex_Vb\t ---> 0x{hex_Vb}")

# Costruisco la stringa da scrivere
STX = "02"
# Command = "HBV"
# Command = binascii.hexlify(b'HBV')
Command = "HBV".encode("utf-8").hex()
Data = hex_Vb.upper() # Tensione a 4 cifre hex (2 byte?)
ETX = "03"
#CheckSum = f"{( int(STX, 16) + int(Command[0:2], 16) + int(Command[2:4], 16) + int(Command[4:], 16) + int(Data[0], 16) + int(Data[1], 16) + int(Data[2], 16) + int(Data[3], 16) + int(ETX, 16)) :04x}".upper()
CheckSum = int(STX, 16)

CheckSum += int(Command[0:2], 16)
CheckSum += int(Command[2:4], 16)
CheckSum += int(Command[4:], 16)

CheckSum += int(Data[0].encode("utf-8").hex(), 16)
CheckSum += int(Data[1].encode("utf-8").hex(), 16)
CheckSum += int(Data[2].encode("utf-8").hex(), 16)
CheckSum += int(Data[3].encode("utf-8").hex(), 16)

CheckSum += int(ETX, 16)

CheckSum = f"{CheckSum:04x}".upper()

CR = "0D"

CS1 = CheckSum[-2].encode("utf-8").hex()
CS2 = CheckSum[-1].encode("utf-8").hex()


print("Computing checksum....")
"""
print(f"{int(STX, 16)}")

print(f"{int(Command[0:2])}")
print(f"{int(Command[2:4])}")
print(f"{int(Command[4:])}")

print(f"{Data[0]}")
print(f"{Data[1]}")
print(f"{Data[2]}")
print(f"{Data[3]}")

print(f"{int(ETX, 16)}")
"""
print(f"int(STX, 16)\t{int(STX, 16)}")

print(f"int(Command[0:2], 16)\t{int(Command[0:2], 16)}")
print(f"int(Command[2:4], 16)\t{int(Command[2:4], 16)}")
print(f"int(Command[4:], 16)\t{int(Command[4:], 16)}")

print(f"int(Data[0], 16)\t{int(Data[0], 16)}")
print(f"int(Data[1], 16)\t{int(Data[1], 16)}")
print(f"int(Data[2], 16)\t{int(Data[2], 16)}")
print(f"int(Data[3], 16)\t{int(Data[3], 16)}")

print(f"int(ETX, 16)\t{int(ETX, 16)}")

strToWrite = STX + Command + Data[0].encode("utf-8").hex() + Data[1].encode("utf-8").hex() + Data[2].encode("utf-8").hex() + Data[3].encode("utf-8").hex() + ETX + CS1 + CS2 + CR

print()
print("Costruisco la stringa da scrivere")
print(f"STX\t{STX}")
print(f"Command\t{Command}")
print(f"Data\t{Data}")
print(f"ETX\t{ETX}")
print(f"CheckSum\t{CheckSum}")
print(f"CS1\t{CS1}")
print(f"CS2\t{CS2}")
print(f"CR\t{CR}")

binToWrite = bytearray.fromhex(strToWrite)

# print()
# for i in binToWrite: print(hex(i))



print()
print(f"Scriverò \n{strToWrite}")
print(f"Scriverò \n{binToWrite}")


with serial.Serial() as ser:
    ser.baudrate = 38400
    ser.port = 'COM7'
    
    ser.parity = serial.PARITY_EVEN
    ser.bytesize = serial.EIGHTBITS
    ser.stopbits = serial.STOPBITS_ONE
    ser.timeout = 15
    
    ser.open()
    
    #ser.write("HOF".encode())
    ser.write(binToWrite)
    #ser.write("HON".encode())

    s = ser.read(40)
    print(f"s: --> {s}")

    
print("Ho scritto")


