#!/usr/bin/env python3
"""
This script demonstrates how to use the HST command 
From the manual, this is intended for setting the temperature correcion factor
Actually it seems allows for writing into the EEPROM...

This command requires 4*6 = 24 bytes of data, as shown below

Data:
- Secondly high temperature side coefficient
- Secondly low temperature side coefficient
- Primary high temperature side coefficient
- Primary low temperature side coefficient
- Reference voltage
- Reference temperature


In this script I set only the last two parameters
"""

from lib_c11204 import *
import serial

# Valori che voglio settare
Vb = 61 # Volt
Tb = 26 # °c


# Converto in ADC e tronco a 4 caratteri
Vb_ADC = f"{(Tension_VtoADC(Vb) & 0xffff):04X}"
Tb_ADC = f"{(Temperature_CtoADC(Tb) & 0xffff):04X}"

binToWrite = encodeCommand("HST", "0000"*4 + Vb_ADC + Tb_ADC)

print(f"Scriverò \n{binToWrite}")

with serial.Serial() as ser:
    
    # Serial port settings
    ser.baudrate = 38400
    ser.port = 'COM7'
    
    ser.parity = serial.PARITY_EVEN
    ser.bytesize = serial.EIGHTBITS
    ser.stopbits = serial.STOPBITS_ONE
    ser.timeout = 15
    
    # Open serial port
    ser.open()
    
    # Write my command
    ser.write(binToWrite)

    #s = ser.read(40)
    time.sleep(1)
    bytesToRead = ser.inWaiting()
    s = ser.read(bytesToRead)
    print(f"s: --> {s}")

    
print("Ho scritto")
